var path = require('path');

const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
    mode: 'production',
    entry: './src/index',
    devServer: {
        contentBase: './public',
        watchContentBase: true
    },
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'tmce-bs-cfg.bundle.js'
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json']
    },
    module: {
        rules: [{
            // Include ts, tsx, js, and jsx files.
            test: /\.(ts|js)x?$/,
            exclude: [
                path.resolve(__dirname, 'node_modules'),
                path.resolve(__dirname, 'public/tinymce')
            ],
            loader: 'babel-loader',
        }],
    },
    plugins: [
        new CopyPlugin({
            patterns: [
                { from: 'node_modules/tinymce', to: 'tinymce' },
                { from: 'node_modules/tinymce-i18n/langs/ru.js', to: 'tinymce/langs/ru.js' }
            ],
        }),
    ]
};