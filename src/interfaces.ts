export interface ITemplate {
    title: string;
    content?: string;
    url?: string;
    description?: string;
}
