import { ITemplate } from './interfaces';
import plugins from './plugins';
import templates from './templates';
import { Direction, searchEl, toggleParentClass, updateIndent, updateMargin, updatePadding } from './utils';

class TinyBsConfig {
    templates: ITemplate[] = templates;
    plugins: string[] = plugins;

    common = {
        language: 'ru',
        width: '100%',
        height: 640,
        branding: false,
        verify_html: false,
        image_advtab: true,
        toolbar1:
            'insertfile undo redo | styleselect | forecolor backcolor emoticons | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
        toolbar2: 'code | template | print preview media | link image | invert togglecontainer | cover | indentplus indentminus hindentplus hindentminus',
        toolbar3: 'mtplus mtminus mbplus mbminus mrplus mrminus mlplus mlminus | ptplus ptminus pbplus pbminus prplus prminus plplus plminus',
    };

    setup(editor: any) {
        editor.ui.registry.addButton('cover', {
            text: ' Фон',
            tooltip: 'Фоновое изображение контейнера',
            icon: 'image',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;

                const cssClass = 'bb-cover';
                let targetUrl = '';
                let $target = searchEl($element, cssClass);
                if ($target) targetUrl = $target.style.backgroundImage.slice(4, -1).replace(/['"]/g, '');

                let url = globalThis.prompt('Адрес фонового изображения', targetUrl);
                $target.style.backgroundImage = url ? `url('${url}')` : '';
                $target.classList[url ? 'add' : 'remove'](cssClass);
                $target.removeAttribute('data-mce-style');
                if (![...$target.classList].length) $target.removeAttribute('class');
            },
        });
        editor.ui.registry.addButton('indentminus', {
            text: ' Y-',
            tooltip: 'Уменьшить вертикальные отступы контейнера',
            icon: 'chevron-down',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updateIndent($element, true, false);
            },
        });
        editor.ui.registry.addButton('indentplus', {
            text: ' Y+',
            tooltip: 'Увеличить вертикальные отступы контейнера',
            icon: 'chevron-up',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updateIndent($element, true, true);
            },
        });
        editor.ui.registry.addButton('hindentminus', {
            text: ' X-',
            tooltip: 'Уменьшить горизонтальные отступы контейнера',
            icon: 'chevron-left',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updateIndent($element, false, false);
            },
        });
        editor.ui.registry.addButton('hindentplus', {
            text: ' X+',
            tooltip: 'Увеличить горизонтальные отступы контейнера',
            icon: 'chevron-right',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updateIndent($element, false, true);
            },
        });
        editor.ui.registry.addButton('invert', {
            text: ' Инверсия',
            tooltip: 'Инверсия фона контейнера, темный/светлый',
            icon: 'invert',
            onAction: function () {
                toggleParentClass(editor, 'bb-dark');
            },
        });
        editor.ui.registry.addButton('togglecontainer', {
            text: ' Контейнер',
            tooltip: 'Переключение ширины контейнера, полная/ограниченная',
            icon: 'sourcecode',
            onAction: function () {
                toggleParentClass(editor, 'container');
            },
        });
        /** Bootstrap margins */
        editor.ui.registry.addButton('mtplus', {
            text: ` m${Direction.TOP}+`,
            tooltip: 'Увеличить внешний отступ сверху',
            icon: 'chevron-up',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updateMargin($element, Direction.TOP, true);
            },
        });
        editor.ui.registry.addButton('mtminus', {
            text: ` m${Direction.TOP}-`,
            tooltip: 'Уменьшить внешний отступ сверху',
            icon: 'chevron-down',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updateMargin($element, Direction.TOP, false);
            },
        });
        editor.ui.registry.addButton('mbplus', {
            text: ` m${Direction.BOTTOM}+`,
            tooltip: 'Увеличить внешний отступ снизу',
            icon: 'chevron-up',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updateMargin($element, Direction.BOTTOM, true);
            },
        });
        editor.ui.registry.addButton('mbminus', {
            text: ` m${Direction.BOTTOM}-`,
            tooltip: 'Уменьшить внешний отступ снизу',
            icon: 'chevron-down',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updateMargin($element, Direction.BOTTOM, false);
            },
        });
        editor.ui.registry.addButton('mrplus', {
            text: ` m${Direction.RIGHT}+`,
            tooltip: 'Увеличить внешний отступ справа',
            icon: 'chevron-up',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updateMargin($element, Direction.RIGHT, true);
            },
        });
        editor.ui.registry.addButton('mrminus', {
            text: ` m${Direction.RIGHT}-`,
            tooltip: 'Уменьшить внешний отступ справа',
            icon: 'chevron-down',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updateMargin($element, Direction.RIGHT, false);
            },
        });
        editor.ui.registry.addButton('mlplus', {
            text: ` m${Direction.LEFT}+`,
            tooltip: 'Увеличить внешний отступ слева',
            icon: 'chevron-up',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updateMargin($element, Direction.LEFT, true);
            },
        });
        editor.ui.registry.addButton('mlminus', {
            text: ` m${Direction.LEFT}-`,
            tooltip: 'Уменьшить внешний отступ слева',
            icon: 'chevron-down',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updateMargin($element, Direction.LEFT, false);
            },
        });
        /** Bootstrap paddings */
        editor.ui.registry.addButton('ptplus', {
            text: ` p${Direction.TOP}+`,
            tooltip: 'Увеличить внутренний отступ сверху',
            icon: 'chevron-up',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updatePadding($element, Direction.TOP, true);
            },
        });
        editor.ui.registry.addButton('ptminus', {
            text: ` p${Direction.TOP}-`,
            tooltip: 'Уменьшить внутренний отступ сверху',
            icon: 'chevron-down',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updatePadding($element, Direction.TOP, false);
            },
        });
        editor.ui.registry.addButton('pbplus', {
            text: ` p${Direction.BOTTOM}+`,
            tooltip: 'Увеличить внутренний отступ снизу',
            icon: 'chevron-up',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updatePadding($element, Direction.BOTTOM, true);
            },
        });
        editor.ui.registry.addButton('pbminus', {
            text: ` p${Direction.BOTTOM}-`,
            tooltip: 'Уменьшить внутренний отступ снизу',
            icon: 'chevron-down',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updatePadding($element, Direction.BOTTOM, false);
            },
        });
        editor.ui.registry.addButton('prplus', {
            text: ` p${Direction.RIGHT}+`,
            tooltip: 'Увеличить внутренний отступ справа',
            icon: 'chevron-up',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updatePadding($element, Direction.RIGHT, true);
            },
        });
        editor.ui.registry.addButton('prminus', {
            text: ` p${Direction.RIGHT}-`,
            tooltip: 'Уменьшить внутренний отступ справа',
            icon: 'chevron-down',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updatePadding($element, Direction.RIGHT, false);
            },
        });
        editor.ui.registry.addButton('plplus', {
            text: ` p${Direction.LEFT}+`,
            tooltip: 'Увеличить внутренний отступ слева',
            icon: 'chevron-up',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updatePadding($element, Direction.LEFT, true);
            },
        });
        editor.ui.registry.addButton('plminus', {
            text: ` p${Direction.LEFT}-`,
            tooltip: 'Уменьшить внутренний отступ слева',
            icon: 'chevron-down',
            onAction: function () {
                let $element = editor.selection.getStart();
                if (!$element) return;
                updatePadding($element, Direction.LEFT, false);
            },
        });
    }
}

declare global {
    interface Window {
        __TINY_BS_CONFIG__: TinyBsConfig;
    }
}

window.__TINY_BS_CONFIG__ = window.__TINY_BS_CONFIG__ ?? new TinyBsConfig();
