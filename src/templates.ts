import { ITemplate } from './interfaces';

const templates: ITemplate[] = [
    {
        title: 'Контейнер с заливкой фона: центрированный',
        url: 'container-bg-centered.html',
    },
    {
        title: 'Контейнер с заливкой фона: тёмный, центрированный',
        url: 'container-bg-dark-centered.html',
    },
    {
        title: 'Контейнер с заливкой фона: тёмный, правый',
        url: 'container-bg-dark-right.html',
    },
    {
        title: 'Контейнер с заливкой фона: тёмный, левый',
        url: 'container-bg-dark-left.html',
    },
    {
        title: 'Контейнер с заливкой фона и изображением: тёмный, правый',
        url: 'container-bg-img-dark-right.html',
    },
    {
        title: 'Контейнер с заливкой фона и изображением: тёмный, левый',
        url: 'container-bg-img-dark-left.html',
    },
    {
        title: 'Характеристики: набор карточек, базовая тема',
        url: 'features-clean.html',
    },
    {
        title: 'Характеристики: набор карточек, синяя тема',
        url: 'features-blue.html',
    },
    {
        title: 'Характеристики: набор крупных карточек',
        url: 'features-boxed.html',
    },
    {
        title: 'Текст: заголовок и подзаголовок',
        url: 'title-subtitle.html',
    },
    {
        title: 'Текст и изображение: текст слева',
        url: 'text-img-text-left.html',
    },
    {
        title: 'Текст и изображение: текст справа',
        url: 'text-img-text-right.html',
    },
    {
        title: 'Текст и изображение: тёмный блок, текст слева',
        url: 'text-img-bg-text-left.html',
    },
    {
        title: 'Текст и изображение: тёмный блок, текст справа',
        url: 'text-img-bg-text-right.html',
    },
    {
        title: 'Галерея Lightbox',
        url: 'lightbox.html',
    },
    {
        title: 'Карусель: стандартная',
        url: 'carousel.html',
    },
    {
        title: 'Карусель: карточки, 3 на слайд',
        url: 'carousel-3-cards-per-slide.html',
    },
    {
        title: 'Таблица: адаптивная',
        url: 'table-responsive.html',
    },
    {
        title: 'Таблица: адаптивная, тёмная',
        url: 'table-dark-responsive.html',
    },
    {
        title: 'Карточки: изображение, заголовок, текст',
        url: 'cards-img-title-text.html',
    },
    {
        title: 'Карточки: изображение, заголовок, текст, тёмная тема',
        url: 'cards-img-title-text-dark.html',
    },
    {
        title: 'Видео: адаптивный режим',
        url: 'video-responsive.html',
    },
    {
        title: 'TPP: Описание',
        url: 'tpp-description.html',
    },
    {
        title: 'TPP: Спецификации',
        url: 'tpp-features.html',
    },
];

templates.forEach(tpl => {
    tpl.url = `templates/${tpl.url}`;
    tpl.description = '';
});
export default templates;
