enum Direction {
    TOP = 't',
    BOTTOM = 'b',
    RIGHT = 'r',
    LEFT = 'l',
}

enum Indent {
    PADDING,
    MARGIN,
}

const IndentType = new Map<Indent, string>([
    [Indent.PADDING, 'p'],
    [Indent.MARGIN, 'm'],
]);

const isTargetElement = ($el: HTMLElement) => {
    if (!$el) return false;

    return ['data-container', 'data-tpl'].some(attr => $el.hasAttribute(attr));
};

const searchEl = ($el: HTMLElement, cssClass: string = ''): HTMLElement => {
    let classList = $el.classList;
    let $parent: HTMLElement = $el.parentNode as HTMLElement;

    if ($parent.tagName.toLowerCase() === 'body') return $el;

    const isRecursive = classList && !classList.contains(cssClass) && $parent && !isTargetElement($parent);

    return isRecursive ? searchEl($parent) : $el;
};

const updateIndent = ($el: HTMLElement, vertical: boolean, increase: boolean): void => {
    const start = 'p' + (vertical ? 'y' : 'x') + '-';
    const step = vertical ? 10 : 1;
    let value: number;
    let indentCssClass = [...$el.classList].find(c => c.indexOf(start) !== -1);

    if (indentCssClass) {
        indentCssClass = indentCssClass.toString();
        if (increase && indentCssClass === start + (vertical ? '100v' : 7)) return;
        if (!increase && indentCssClass === start + (vertical ? '10v' : 0)) {
            $el.classList.remove(indentCssClass);
            return;
        }
        value = +indentCssClass.replace(start, '').replace('v', '') | 0;
        value = increase ? value + step : value - step;

        $el.classList.remove(indentCssClass);
        $el.classList.add(start + value + (vertical ? 'v' : ''));
    } else {
        if (increase) $el.classList.add(start + (vertical ? '10v' : 0));
    }
};

const toggleParentClass = (editor: any, className: string): void => {
    let $element = editor.selection.getStart();
    if (!$element) return;

    let $target = searchEl($element, className);
    $target.classList[$target.classList.contains(className) ? 'remove' : 'add'](className);
    if (![...$target.classList].length) $target.removeAttribute('class');
};

const searchParentByStartCssClass = ($el: HTMLElement, start: string): HTMLElement => {
    if (isTargetElement($el)) return $el;

    const indentCssClass = [...$el.classList].find(c => c.indexOf(start) !== -1);
    if (indentCssClass) return $el;

    let $parent: HTMLElement = $el.parentNode as HTMLElement;
    if ($parent.tagName.toLowerCase() === 'body') return $el;

    return searchParentByStartCssClass($parent, start);
};

const updateCssClass = ($el: HTMLElement, direction: Direction, increase: boolean, type: Indent): void => {
    const step = 1;
    const min = 0;
    const max = 7;

    const start = IndentType.get(type) + direction + '-';
    const $target = searchParentByStartCssClass($el, start);

    if (!$target) return;

    let indentCssClass = [...$target.classList].find(c => c.indexOf(start) !== -1);
    let value;

    if (indentCssClass) {
        indentCssClass = indentCssClass.toString();
        value = +indentCssClass.replace(start, '') | 0;
        value = increase ? value + step : value - step;
        if (value > max) value = max;

        $target.classList.remove(indentCssClass);
        if (value >= min) $target.classList.add(start + value);
    } else {
        if (increase) $target.classList.add(start + min);
    }
};

const updatePadding = ($el: HTMLElement, direction: Direction, increase: boolean) => updateCssClass($el, direction, increase, Indent.PADDING);

const updateMargin = ($el: HTMLElement, direction: Direction, increase: boolean) => updateCssClass($el, direction, increase, Indent.MARGIN);

export { searchEl, updateIndent, toggleParentClass, updatePadding, updateMargin, Direction };
